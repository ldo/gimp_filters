#!/usr/bin/python3
#+
# This script is a GIMP plugin that generates a "brushed metal" pattern.
#
# Copyright 2011 by Lawrence D'Oliveiro <ldo@geek-central.gen.nz>. This
# script is licensed CC0
# <https://creativecommons.org/publicdomain/zero/1.0/>; do with it
# what you will.
#-

import libgimp2
from libgimp2 import \
    GIMP, \
    pdb, \
    pdb1, \
    pdbpi

def do_effect(the_image, the_drawable) :
    pdb.gimp_image_undo_group_start(the_image)
    the_layer = pdb1.gimp_layer_new \
      (
        image = the_image,
        width = the_drawable.width,
        height = the_drawable.height,
        type = GIMP.RGB_IMAGE,
        name = "Brushed Metal",
        opacity = 100,
        mode = GIMP.LAYER_MODE_NORMAL
      )
    pdb.gimp_image_add_layer(the_image, the_layer, position = -1)
    pdbpi.plug_in_hsv_noise \
      (
        image = the_image,
        drawable = the_layer,
        holdness = 1,
        hue_distance = 0,
        saturation_distance = 0,
        value_distance = 226
      )
    pdb.gimp_levels \
      (
        drawable = the_layer,
        channel = GIMP.HISTOGRAM_VALUE,
        low_input = 0,
        high_input = 0,
        gamma = 1.0,
        low_output = 64,
        high_output = 192
      )
    pdbpi.plug_in_mblur \
      (
        image = the_image,
        drawable = the_layer,
        type = 0, # linear
        length = 39,
        angle = 0,
        center_x = 0, # not relevant for linear blur
        center_y = 0 # not relevant for linear blur
      )
    pdbpi.plug_in_unsharp_mask \
      (
        image = the_image,
        drawable = the_layer,
        radius = 46,
        amount = 1,
        threshold = 0
      )
    pdb.gimp_image_undo_group_end(the_image)
#end do_effect

libgimp2.plugin_install \
  (
    name = "python_fu_brushed_metal",
    blurb = "Generate a brushed-metal texture",
    help = "Generates a new image layer filled with the texture.",
    author = "Lawrence D'Oliveiro",
    copyright = "©2011 by Lawrence D’Oliveiro",
    date = "2022 June 5",
    image_types = "RGB*, GRAY*", # same as Gaussian blur
    placement = libgimp2.UI_PLACEMENT.IMAGE,
    action = do_effect,
    item_label = "Brushed Metal",
    menu_name = "/Filters/Render",
  )

libgimp2.main()
