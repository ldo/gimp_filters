#!/usr/bin/python3
#+
# This script is a GIMP plugin that simulates a simple diffusion-filter effect.
#
# Copyright 2009 by Lawrence D'Oliveiro <ldo@geek-central.gen.nz>. This
# script is licensed CC0
# <https://creativecommons.org/publicdomain/zero/1.0/>; do with it
# what you will.
#-

import libgimp2 as gimp
from libgimp2 import \
    GIMP, \
    PARAMTYPE, \
    pdb, \
    pdb1, \
    pdbpi

def do_effect(the_image, drawable, blur_radius, opacity) :
    pdb.gimp_image_undo_group_start(the_image)
    the_layer = pdb1.gimp_layer_copy(pdb1.gimp_image_get_active_layer(the_image), add_alpha = False)
    pdb.gimp_image_add_layer(the_image, the_layer, position = -1)
    # pdb.gimp_layer_set_mode(the_layer, GIMP.LAYER_MODE_DISSOLVE)
    pdbpi.plug_in_gauss_rle(the_image, the_layer, blur_radius, horizontal = True, vertical = True)
    pdb.gimp_layer_set_opacity(the_layer, opacity)
    pdb.gimp_image_merge_down(the_image, the_layer, merge_type = GIMP.CLIP_TO_BOTTOM_LAYER)
    pdb.gimp_image_undo_group_end(the_image)
#end do_effect

gimp.plugin_install \
  (
    name = "python_fu_diffusion",
    blurb = "Effect of shooting through a diffusion filter",
    help = "Works on the whole image (ignores selection).",
    author = "Lawrence D'Oliveiro",
    copyright = "©2009 by Lawrence D’Oliveiro",
    date = "2022 June 3",
    image_types = "RGB*, GRAY*", # same as Gaussian blur
    placement = gimp.UI_PLACEMENT.IMAGE,
    action = do_effect,
    params =
        [
            {
                "type" : PARAMTYPE.FLOAT,
                "name" : "blur_radius",
                "description" : "Blur radius",
                "default" : 6.0,
                "lower" : 0.0,
                "upper" : 50,
            },
            {
                "type" : PARAMTYPE.FLOAT,
                "name" : "opacity",
                "description" : "Effect Opacity (%)",
                "default" : 40,
                "lower" : 0,
                "upper" : 100,
                "entry_style" : gimp.ENTRYSTYLE.SLIDER,
            },
        ],
    menu_name = "/Filters/Blur",
    item_label = "Diffusion...",
  )

gimp.main()
