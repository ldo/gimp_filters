#!/usr/bin/python3
#+
# Implementation of refinement on top of basic Make Seamless filter as
# described by Bowie J Poag here
# <http://www.mail-archive.com/gimp-developer@lists.xcf.berkeley.edu/msg06649.html>.
#
# Copyright 2010 by Lawrence D'Oliveiro <ldo@geek-central.gen.nz>. This
# script is licensed CC0
# <https://creativecommons.org/publicdomain/zero/1.0/>; do with it
# what you will.
#-

import libgimp2
from libgimp2 import \
    GIMP, \
    pdb, \
    pdbpi

def do_effect(the_image, the_drawable) :
    k = 0.15
    drawable_width = the_drawable.width
    drawable_height = the_drawable.height
    border_width = round(drawable_width * k)
    border_height = round(drawable_height * k)
    offsets = pdb.gimp_drawable_offsets(the_drawable)
    pdb.gimp_image_undo_group_start(the_image)
    pdb.gimp_selection_all(the_image)
    pdb.gimp_rect_select \
      (
        image = the_image,
        x = offsets[0],
        y = offsets[1],
        width = drawable_width,
        height = drawable_height,
        operation = GIMP.CHANNEL_OP_REPLACE,
        feather = False, # unimportant
        feather_radius = 0
      )
    pdb.gimp_rect_select \
      (
        image = the_image,
        x = offsets[0] + border_width,
        y = offsets[1] + border_height,
        width = drawable_width - 2 * border_width,
        height = drawable_height - 2 * border_height,
        operation = GIMP.CHANNEL_OP_SUBTRACT,
        feather = True,
        feather_radius = round((border_width + border_height) / 2)
      )
    pdbpi.plug_in_make_seamless(the_image, the_drawable)
    pdb.gimp_selection_none(the_image)
    pdb.gimp_image_undo_group_end(the_image)
#end do_effect

libgimp2.plugin_install \
  (
    name = "python_fu_conservative_make_seamless",
    blurb = "Conservative version of Make Seamless",
    help = "Works on the whole image (ignores selection).",
    author = "Lawrence D'Oliveiro",
    copyright = "©2010 by Lawrence D’Oliveiro",
    date = "2022 June 5",
    image_types = "RGB*, GRAY*", # same as Make Seamless
    placement = libgimp2.UI_PLACEMENT.IMAGE,
    action = do_effect,
    menu_name = "/Filters/Map",
    item_label = "Conservative Make Seamless",
  )

libgimp2.main()
